const express = require('express');
var app = express();

app.get('/', (req, res) => {
    res.send('Hello World');
});

const port = 8080;
app.listen(port, () => {
    console.log('Server listening on port ' + port);
});